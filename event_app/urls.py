"""
URL configuration for event_app project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import include, path
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework.routers import DefaultRouter

import frontend.views as frontend
from events.views import EventViewSet

from .views import ping

router = DefaultRouter()
router.register(r"events", EventViewSet)

urlpatterns = [
    # frontend
    path("home/", frontend.home, name="home"),
    path("register/", frontend.register, name="register"),
    path("login/", frontend.login, name="login"),
    # admin
    path("admin/", admin.site.urls),
    # Auth
    path("api/auth/", include("dj_rest_auth.urls")),
    path("api/registration/", include("dj_rest_auth.registration.urls")),
    # API endpoints
    path("api/", include(router.urls)),
    # Ping
    path("ping/", ping, name="ping"),
    # Swagger
    path("api/schema/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "api/docs/",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="swagger-ui",
    ),
]
