from django.contrib import admin

from .models import Event, Registration


class EventAdmin(admin.ModelAdmin):
    pass


class RegistrationAdmin(admin.ModelAdmin):
    pass


admin.site.register(Event, EventAdmin)
admin.site.register(Registration, RegistrationAdmin)
