from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class Event(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    creator = models.ForeignKey(
        User, related_name="created_events", on_delete=models.CASCADE
    )
    attendees = models.ManyToManyField(
        User, through="Registration", related_name="attended_events"
    )
    capacity = models.PositiveIntegerField(default=100)
    EVENT_TYPES = (
        ("CONFERENCE", "Conference"),
        ("SEMINAR", "Seminar"),
        ("MEETUP", "Meetup"),
        ("WORKSHOP", "Workshop"),
        ("WEBINAR", "Webinar"),
        ("TRADE_SHOW", "Trade Show"),
        ("COMMUNITY_EVENT", "Community Event"),
        ("SOCIAL_EVENT", "Social Event"),
        ("CONCERT", "Concert"),
        ("OTHER", "Other"),
    )
    type = models.CharField(max_length=20, choices=EVENT_TYPES, default="MEETUP")

    def __str__(self):
        return self.name

    @property
    def is_past_event(self):
        return self.end_date < timezone.now()

    @property
    def places_left(self):
        return self.capacity - self.attendees.count()


class Registration(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    registration_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ("user", "event")

    def __str__(self):
        return f"{self.user.username} - {self.event.name}"
