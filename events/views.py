from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .filters import EventFilter
from .models import Event, Registration
from .permissions import IsCreatorOrReadOnly
from .serializers import EventSerializer


class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    permission_classes = [permissions.IsAuthenticated, IsCreatorOrReadOnly]
    filter_backends = (DjangoFilterBackend,)
    filterset_class = EventFilter

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)

    def get_queryset(self):
        """
        Optionally restricts the returned events to those created by the user,
        by filtering against a 'created_by_me' query parameter in the URL.
        """
        queryset = Event.objects.all()
        created_by_me = self.request.query_params.get("created_by_me", None)
        if created_by_me is not None and created_by_me.lower() == "true":
            queryset = queryset.filter(creator=self.request.user)
        return queryset

    @action(
        detail=True, methods=["post"], permission_classes=[permissions.IsAuthenticated]
    )
    def register(self, request, pk=None):
        event = get_object_or_404(Event, pk=pk)
        if event.is_past_event:
            return Response(
                {"error": "Cannot register for past events"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        if event.places_left == 0:
            return Response(
                {"error": "Event is full"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        registration, created = Registration.objects.get_or_create(
            user=request.user, event=event
        )
        if created:
            return Response(
                {"success": "Registered successfully"}, status=status.HTTP_201_CREATED
            )
        else:
            return Response(
                {"error": "Already registered"}, status=status.HTTP_400_BAD_REQUEST
            )

    @action(
        detail=True, methods=["post"], permission_classes=[permissions.IsAuthenticated]
    )
    def unregister(self, request, pk=None):
        event = get_object_or_404(Event, pk=pk)
        if event.is_past_event:
            return Response(
                {"error": "Cannot unregister from past events"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        try:
            registration = Registration.objects.get(user=request.user, event=event)
            registration.delete()
            return Response(
                {"success": "Unregistered successfully"},
                status=status.HTTP_204_NO_CONTENT,
            )
        except Registration.DoesNotExist:
            return Response(
                {"error": "Not registered"}, status=status.HTTP_400_BAD_REQUEST
            )
