import django_filters
from django.utils import timezone

from .models import Event


class EventFilter(django_filters.FilterSet):
    past_events = django_filters.BooleanFilter(
        method="filter_past_events", label="Past Events"
    )
    future_events = django_filters.BooleanFilter(
        method="filter_future_events", label="Future Events"
    )
    start_date_after = django_filters.DateTimeFilter(
        field_name="start_date", lookup_expr="gte", label="Start Date After"
    )
    end_date_before = django_filters.DateTimeFilter(
        field_name="end_date", lookup_expr="lte", label="End Date Before"
    )
    type = django_filters.ChoiceFilter(choices=Event.EVENT_TYPES)

    class Meta:
        model = Event
        fields = ["type", "start_date_after", "end_date_before"]

    def filter_past_events(self, queryset, name, value):
        if value:
            return queryset.filter(end_date__lt=timezone.now())
        return queryset

    def filter_future_events(self, queryset, name, value):
        if value:
            return queryset.filter(start_date__gt=timezone.now())
        return queryset
