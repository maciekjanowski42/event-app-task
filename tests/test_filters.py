import datetime

import pytest
from django.utils import timezone

from events.models import Event


@pytest.mark.django_db
def test_filter_past_events(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    add_event(
        name="Past Event",
        start_date=timezone.now() - datetime.timedelta(days=2),
        end_date=timezone.now() - datetime.timedelta(days=1),
        creator=user,
    )
    add_event(
        name="Future Event",
        start_date=timezone.now() + datetime.timedelta(days=5),
        end_date=timezone.now() + datetime.timedelta(days=6),
        creator=user,
    )

    response = client.get("/api/events/", {"past_events": True})
    assert response.status_code == 200
    assert len(response.data) == 1
    assert response.data[0]["name"] == "Past Event"


@pytest.mark.django_db
def test_filter_future_events(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    add_event(
        name="Past Event",
        start_date=timezone.now() - datetime.timedelta(days=2),
        end_date=timezone.now() - datetime.timedelta(days=1),
        creator=user,
    )
    add_event(
        name="Future Event",
        start_date=timezone.now() + datetime.timedelta(days=5),
        end_date=timezone.now() + datetime.timedelta(days=6),
        creator=user,
    )

    response = client.get("/api/events/", {"future_events": True})
    assert response.status_code == 200
    assert len(response.data) == 1
    assert response.data[0]["name"] == "Future Event"


@pytest.mark.django_db
def test_filter_events_by_type(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    add_event(user, name="Meetup Event", type="MEETUP")
    add_event(user, name="Conference Event", type="CONFERENCE")

    response = client.get("/api/events/", {"type": "MEETUP"})
    assert response.status_code == 200
    assert len(response.data) == 1
    assert response.data[0]["name"] == "Meetup Event"


@pytest.mark.django_db
def test_filter_events_start_date_after(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    add_event(
        user,
        name="Event After",
        start_date=timezone.now() + datetime.timedelta(days=3),
        end_date=timezone.now() + datetime.timedelta(days=4),
    )
    add_event(
        user,
        name="Event Before",
        start_date=timezone.now() - datetime.timedelta(days=1),
        end_date=timezone.now() + datetime.timedelta(days=1),
    )

    date_str = (timezone.now() + datetime.timedelta(days=2)).strftime("%Y-%m-%d")
    response = client.get("/api/events/", {"start_date_after": date_str})
    assert response.status_code == 200
    assert len(response.data) == 1
    assert response.data[0]["name"] == "Event After"


@pytest.mark.django_db
def test_filter_events_end_date_before(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    add_event(
        user,
        name="Event Before",
        start_date=timezone.now() - datetime.timedelta(days=2),
        end_date=timezone.now() - datetime.timedelta(days=1),
    )
    add_event(
        user,
        name="Event After",
        start_date=timezone.now() + datetime.timedelta(days=1),
        end_date=timezone.now() + datetime.timedelta(days=3),
    )

    date_str = timezone.now().strftime("%Y-%m-%d")
    response = client.get("/api/events/", {"end_date_before": date_str})
    assert response.status_code == 200
    assert len(response.data) == 1
    assert response.data[0]["name"] == "Event Before"


@pytest.mark.django_db
def test_filter_events_combination(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    add_event(
        user,
        name="Past Meetup",
        start_date=timezone.now() - datetime.timedelta(days=3),
        end_date=timezone.now() - datetime.timedelta(days=2),
        type="MEETUP",
    )
    add_event(
        user,
        name="Future Conference",
        start_date=timezone.now() + datetime.timedelta(days=2),
        end_date=timezone.now() + datetime.timedelta(days=3),
        type="CONFERENCE",
    )
    add_event(
        user,
        name="Future Meetup",
        start_date=timezone.now() + datetime.timedelta(days=1),
        end_date=timezone.now() + datetime.timedelta(days=2),
        type="MEETUP",
    )

    response = client.get("/api/events/", {"future_events": True, "type": "MEETUP"})
    assert response.status_code == 200
    assert len(response.data) == 1
    assert response.data[0]["name"] == "Future Meetup"


@pytest.mark.django_db
def test_filter_past_events_false(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    add_event(
        user,
        name="Past Event",
        start_date=timezone.now() - datetime.timedelta(days=2),
        end_date=timezone.now() - datetime.timedelta(days=1),
    )
    add_event(
        name="Future Event",
        start_date=timezone.now() + datetime.timedelta(days=5),
        end_date=timezone.now() + datetime.timedelta(days=6),
        creator=user,
    )

    response = client.get("/api/events/", {"past_events": False})
    assert response.status_code == 200
    assert len(response.data) == Event.objects.count()


@pytest.mark.django_db
def test_filter_future_events_false(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    add_event(
        user,
        name="Past Event",
        start_date=timezone.now() - datetime.timedelta(days=2),
        end_date=timezone.now() - datetime.timedelta(days=1),
    )
    add_event(
        name="Future Event",
        start_date=timezone.now() + datetime.timedelta(days=5),
        end_date=timezone.now() + datetime.timedelta(days=6),
        creator=user,
    )

    response = client.get("/api/events/", {"future_events": False})
    assert response.status_code == 200
    assert len(response.data) == Event.objects.count()
