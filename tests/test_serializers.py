import pytest

from events.serializers import EventSerializer, UserSerializer


@pytest.mark.django_db
def test_valid_user_serializer():
    valid_serializer_data = {
        "username": "testuser",
        "email": "test@email.com",
    }
    serializer = UserSerializer(data=valid_serializer_data)
    assert serializer.is_valid()
    assert serializer.validated_data == valid_serializer_data
    assert serializer.data == valid_serializer_data
    assert serializer.errors == {}


@pytest.mark.django_db
def test_invalid_user_serializer():
    invalid_serializer_data = {
        "email": "test@email.com",
    }
    serializer = UserSerializer(data=invalid_serializer_data)
    assert not serializer.is_valid()
    assert serializer.validated_data == {}
    assert serializer.data == invalid_serializer_data
    assert serializer.errors == {"username": ["This field is required."]}


def test_valid_event_serializer():
    valid_serializer_data = {
        "name": "Test Event",
        "description": "Test Description",
        "start_date": "2024-04-15T08:20:00Z",
        "end_date": "2024-04-16T08:20:00Z",
        "creator": {"id": 1, "username": "testuser", "email": ""},
        "attendees": [],
        "capacity": 10,
    }
    serializer = EventSerializer(data=valid_serializer_data)
    assert serializer.is_valid()
    assert serializer.errors == {}


def test_invalid_event_serializer():
    invalid_serializer_data = {
        "description": "Test Description",
        "start_date": "2024-04-15T08:20:00Z",
        "end_date": "2024-04-16T08:20:00Z",
        "creator": {"id": 1, "username": "testuser", "email": ""},
        "attendees": [],
        "capacity": 10,
    }
    serializer = EventSerializer(data=invalid_serializer_data)
    assert not serializer.is_valid()
    assert serializer.errors == {"name": ["This field is required."]}
