import datetime

import pytest

from events.models import Event


@pytest.mark.django_db
def test_add_event(get_authorized_client, add_user):
    client = get_authorized_client(add_user())
    events = Event.objects.all()
    assert len(events) == 0

    resp = client.post(
        "/api/events/",
        {
            "name": "Test Event",
            "description": "test event",
            "start_date": "2024-04-15T08:20",
            "end_date": "2024-04-16T08:20",
        },
    )
    assert resp.status_code == 201
    assert resp.data["name"] == "Test Event"
    assert resp.data["creator"]["id"] == 1
    assert resp.data["attendees"] == []
    events = Event.objects.all()
    assert len(events) == 1


@pytest.mark.django_db
def test_add_event_invalid_json(get_authorized_client, add_user):
    client = get_authorized_client(add_user())
    events = Event.objects.all()
    assert len(events) == 0

    resp = client.post("/api/events/", {})
    assert resp.status_code == 400
    events = Event.objects.all()
    assert len(events) == 0


@pytest.mark.django_db
def test_add_event_invalid_json_keys(get_authorized_client, add_user):
    client = get_authorized_client(add_user())
    events = Event.objects.all()
    assert len(events) == 0

    resp = client.post(
        "/api/events/",
        {
            "name": "test",
            "description": "test event",
        },
    )
    assert resp.status_code == 400
    events = Event.objects.all()
    assert len(events) == 0


@pytest.mark.django_db
def test_get_single_event(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    event = add_event(creator=user)
    resp = client.get(f"/api/events/{event.id}/")
    assert resp.status_code == 200
    assert resp.data["name"] == "Test Event"
    assert resp.data["creator"]["id"] == 1
    assert resp.data["attendees"] == []


@pytest.mark.django_db
def test_get_single_event_incorrect_id(get_authorized_client, add_user):
    client = get_authorized_client(add_user())
    resp = client.get("/api/events/foo/")
    assert resp.status_code == 404


@pytest.mark.django_db
def test_get_all_events(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    add_event(creator=user)
    add_event(creator=add_user(username="differentuser"), name="different event")
    resp = client.get("/api/events/")
    assert resp.status_code == 200
    assert len(resp.data) == 2
    assert resp.data[0]["name"] == "Test Event"
    assert resp.data[1]["name"] == "different event"


@pytest.mark.django_db
def test_get_user_events(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    add_event(creator=user)
    add_event(creator=add_user(username="differentuser"), name="different event")
    resp = client.get("/api/events/?created_by_me=true")
    assert resp.status_code == 200
    assert len(resp.data) == 1
    assert resp.data[0]["name"] == "Test Event"


@pytest.mark.django_db
def test_update_event(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    event = add_event(creator=user)

    resp = client.put(
        f"/api/events/{event.id}/",
        {
            "name": "New Event Name",
            "description": event.description,
            "start_date": event.start_date,
            "end_date": event.end_date,
        },
    )
    assert resp.status_code == 200
    assert resp.data["name"] == "New Event Name"


@pytest.mark.django_db
def test_update_event_invalid_json(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    event = add_event(creator=user)

    resp = client.put(f"/api/events/{event.id}/", {})
    assert resp.status_code == 400


@pytest.mark.django_db
def test_update_event_invalid_json_keys(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    event = add_event(creator=user)

    resp = client.put(
        f"/api/events/{event.id}/",
        {
            "name": "New Event Name",
        },
    )
    assert resp.status_code == 400


@pytest.mark.django_db
def test_update_event_invalid_permission(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    event = add_event(creator=add_user(username="differentuser"))

    resp = client.put(
        f"/api/events/{event.id}/",
        {
            "name": "New Event Name",
            "description": event.description,
            "start_date": event.start_date,
            "end_date": event.end_date,
        },
    )
    assert resp.status_code == 403


@pytest.mark.django_db
def test_register_event(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    event = add_event(
        user,
        start_date=datetime.datetime(datetime.MAXYEAR, 12, 30, 23, 59, 59),
        end_date=datetime.datetime(datetime.MAXYEAR, 12, 31, 23, 59, 59),
    )

    resp = client.post(f"/api/events/{event.id}/register/")
    assert resp.status_code == 201
    assert resp.data["success"] == "Registered successfully"
    event.refresh_from_db()
    assert event.attendees.first() == user

    # test already registered
    resp = client.post(f"/api/events/{event.id}/register/")
    assert resp.status_code == 400
    assert resp.data["error"] == "Already registered"


@pytest.mark.django_db
def test_register_event_past_event(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    event = add_event(
        user,
        start_date=datetime.datetime(datetime.MAXYEAR, 12, 30, 23, 59, 59),
        end_date=datetime.datetime(datetime.MAXYEAR, 12, 31, 23, 59, 59),
        capacity=1,
    )
    event.attendees.add(add_user(username="user2"))

    resp = client.post(f"/api/events/{event.id}/register/")
    assert resp.status_code == 400
    assert resp.data["error"] == "Event is full"
    assert event.places_left == 0


@pytest.mark.django_db
def test_register_event_max_capacity(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    event = add_event(user)

    resp = client.post(f"/api/events/{event.id}/register/")
    assert resp.status_code == 400
    assert resp.data["error"] == "Cannot register for past events"


@pytest.mark.django_db
def test_unregister_event(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    event = add_event(
        user,
        start_date=datetime.datetime(datetime.MAXYEAR, 12, 30, 23, 59, 59),
        end_date=datetime.datetime(datetime.MAXYEAR, 12, 31, 23, 59, 59),
        capacity=1,
    )
    event.attendees.add(user)

    resp = client.post(f"/api/events/{event.id}/unregister/")
    assert resp.status_code == 204
    assert resp.data["success"] == "Unregistered successfully"
    event.refresh_from_db()
    assert event.attendees.count() == 0
    assert event.places_left == 1


@pytest.mark.django_db
def test_unregister_event_past_event(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    event = add_event(user)

    resp = client.post(f"/api/events/{event.id}/unregister/")
    assert resp.status_code == 400
    assert resp.data["error"] == "Cannot unregister from past events"


@pytest.mark.django_db
def test_unregister_event_not_registered(get_authorized_client, add_user, add_event):
    user = add_user()
    client = get_authorized_client(user)
    event = add_event(
        user,
        start_date=datetime.datetime(datetime.MAXYEAR, 12, 30, 23, 59, 59),
        end_date=datetime.datetime(datetime.MAXYEAR, 12, 31, 23, 59, 59),
    )

    resp = client.post(f"/api/events/{event.id}/unregister/")
    assert resp.status_code == 400
    assert resp.data["error"] == "Not registered"
