import json

import pytest
from django.contrib.auth.models import User
from rest_framework.test import APIClient

from events.models import Event

TEST_PASSWORD = "strong_password"


@pytest.fixture(scope="function")
def add_user():
    def _add_user(
        username="test_user", email="test@example.com", password=TEST_PASSWORD
    ):
        return User.objects.create_user(
            username=username, email=email, password=password
        )

    return _add_user


@pytest.fixture(scope="function")
def add_event():
    def _add_event(
        creator,
        name="Test Event",
        description="Test Description",
        start_date="2021-10-10T10:00:00",
        end_date="2021-10-11T10:00:00",
        capacity=100,
        type="MEETUP",
    ):
        return Event.objects.create(
            name=name,
            description=description,
            start_date=start_date,
            end_date=end_date,
            creator=creator,
            capacity=capacity,
            type=type,
        )

    return _add_event


@pytest.fixture(scope="function")
def get_authorized_client():
    def _get_authorized_client(user):
        client = APIClient()
        login_response = client.post(
            "/api/auth/login/", {"username": user.username, "password": TEST_PASSWORD}
        )
        access_token = json.loads(login_response.content)["access"]
        client.credentials(HTTP_AUTHORIZATION=f"Bearer {access_token}")
        return client

    return _get_authorized_client
