import pytest

from events.models import Event, Registration


@pytest.mark.django_db
def test_event_model(add_user):
    user = add_user()
    event = Event.objects.create(
        name="Test Event",
        description="Test Description",
        start_date="2021-10-10T10:00:00",
        end_date="2021-10-11T10:00:00",
        creator_id=user.id,
        capacity=10,
    )
    assert event.name == "Test Event"
    assert event.description == "Test Description"
    assert event.start_date == "2021-10-10T10:00:00"
    assert event.end_date == "2021-10-11T10:00:00"
    assert event.creator_id == user.id
    assert event.capacity == 10
    assert str(event) == event.name


@pytest.mark.django_db
def test_registration_model(add_user, add_event):
    user = add_user()
    test_event = add_event(user)
    registration = Registration.objects.create(user_id=user.id, event_id=test_event.id)
    assert registration.user_id == user.id
    assert registration.event_id == test_event.id
    assert (
        str(registration) == f"{registration.user.username} - {registration.event.name}"
    )
