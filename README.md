

Event App - A Django Project

This is a Django project for REST API to manage events and register to them

It allows users to create/update/delete events that belong to them.
Users can view all events created by different users and filter them.
Users can register or unregister from the events.

The stack used is Django, Django Rest Framework, Pytest
Front end is served using django for routing, axios for consuming rest api, alpine.js for simple reactivity

Possible future improvements:
- Adding pagination
- Adding more filtering options
- Extending the user model
- Improving the frontend styling, filtering, and authenication process
- Moving frontend to seperate application e.g React
- Dockerizing whole set up 



Prerequisites:

- Python 3.11 or later (https://www.python.org/downloads/)
- Poetry (https://python-poetry.org/docs/)

### Installation:

1. Clone this repository:

```bash
git clone https://gitlab.com/maciekjanowski42/event-app-task
```

2. Navigate to the project directory:

```bash
cd event-app-task
```

3. Install dependencies
```bash
poetry install
```

### Running the development server:

```bash
poetry run python manage.py migrate # run migrations the first time
poetry run python manage.py runserver
```
This will start the Django development server on http://127.0.0.1:8000/ by default.

### Documentation

- The Swagger UI at http://localhost:8000/api/docs/
- The OpenAPI schema at http://localhost:8000/api/schema/

### Running Tests:

```bash
poetry run pytest
```
Running tests with coverage report

```bash
poetry run pytest -p no:warnings --cov=.
```

### Code Quality:

```bash
# run flake8
poetry run flake8 
# run black
poetry run black --exclude=migrations .
# run isort
poetry run isort .
```

### Project Structure

The project is organized with the following directory structure:

```bash
your_project/
├── manage.py
├── pyproject.toml  # Poetry configuration file
├── README.md       # This file
├── tests/           # Directory for your unit tests
│   └── ...
├── events/
│   ├── admin.py
│   ├── apps.py
│   ├── migrations/
│   ├── models.py
│   ├── serializers.py
│   ├── views.py
│   └── ...
```

### Using Frontend (in progress)

```bash
Register /register/
Login /login/
Home /home/
```